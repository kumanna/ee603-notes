all: postclean-stamp

postclean-stamp: _book/index.html _book/ee603-notes.pdf _book/ee603-notes.epub
	# Generate the HTML again to detect the PDF and EPUB
	Rscript -e "bookdown::render_book('index.Rmd', 'bookdown::gitbook')"
	cp Script-Figures/*mp4 _book
	$(RM) ee603-notes.log *pdf *svg *gif
	touch postclean-stamp

FILES = $(shell ls *md) plot_helper.py

_book/index.html: $(FILES)
	cp Script-Figures/*svg .
	cp Script-Figures/*gif .
	cp Static-Figures/*svg .
	Rscript -e "bookdown::render_book('index.Rmd', 'bookdown::gitbook')"

_book/ee603-notes.pdf: $(FILES)
	cp Script-Figures/*svg .
	cp Script-Figures/*gif .
	cp Static-Figures/*svg .
	sh genpdfs.sh
	Rscript -e "bookdown::render_book('index.Rmd', output_format='bookdown::tufte_book2')"

_book/ee603-notes.epub: $(FILES)
	Rscript -e "bookdown::render_book('index.Rmd', 'bookdown::epub_book')"

.PHONY: clean
clean:
	$(RM) -r _book
	$(RM) *-stamp

import pylab as p

def stemplot(n, *x, **kwargs):
    p.rc('text', usetex=True)
    p.rc('font', size=16)
    p.rc('axes', labelsize=16)
    p.rc('legend', fontsize=12)
    fig, ax = p.subplots()
    if 'title' in kwargs:
        ax.set_title(kwargs['title'])
        ax.set_ylabel(r'$x[n]$')
    for xv in x:
        ax.stem(n, xv, use_line_collection=True)
    v = ax.axis()
    ax.axis([n[0], n[-1], v[2], v[3] * 1.1])
    ax.set_xticks(n)
    ax.grid(True)
    ax.set_xlabel(r'$n$')
    if 'filename' in kwargs:
        p.savefig(kwargs['filename'])
    else:
        p.show()
    p.close(fig)


if __name__ == "__main__":
    N = 5
    n = p.r_[-N:N+1]
    x = p.zeros(n.shape)
    x[N] = 1.0
    stemplot(n, x, title=r'$\delta[n]$')

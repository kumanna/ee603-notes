import animate;
import graph;

size(3cm);
settings.outformat="pdf";

defaultpen(fontsize(8pt));
typedef real realfcn(real);
realfcn sinc(real A = 1) {
  return new real(real t) {
    if (t == 0) {
      return A;
    }
    return A * sin(pi * t) / pi / t;
  };
}

picture pic;
size(8cm,4cm,IgnoreAspect);
xaxis("$t$", -3.2, 3.2, Arrow);
xtick("$0$", (0.0,0.0),dir=N,size=0);
xtick("$-2T$", (-2.0,0.0),dir=N,size=0);
xtick("$-T$", (-1.0,0.0),dir=N,size=0);
xtick("$T$", (1.0,0.0),dir=N,size=0);
xtick("$2T$", (2.0,0.0),dir=N,size=0);

void makeplot(picture pic=currentpicture, real s, real tmin, real tmax, pen p=defaultpen, real A=1) {
  draw(pic,shift(s,0)*graph(sinc(A), tmin, tmax),p);
}

animation a = animation(global=true);
makeplot(0, -3, 3);
label("$x_0$", (0,1),N);
a.add();
real A;
int N = 50;
for (int i = 0; i < 2 * N + 1; ++i) {
  save();
  A = (i > N ? (2 * N - i) : i) / N / 2;
  label("$x_1$", (1,A),NE,red);
  makeplot(1, -4, 2,red,A);
  shipout("p" + ((string) (i + 1)));
  restore();
}
a.movie(BBox(1mm),delay=150,
        options="-density 300");

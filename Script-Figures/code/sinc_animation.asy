import animate;
import graph;

size(3cm);
settings.outformat="pdf";

defaultpen(fontsize(8pt));
real sinc(real t) {
  if (t == 0) {
    return 1;
  }
  return sin(pi * t) / pi / t;
}

picture pic;
size(pic,8cm,4cm,IgnoreAspect);
xaxis(pic,"$t$", -3.2, 3.2, Arrow);
xtick(pic,"$0$", (0.0,0.0),dir=N,size=0);
xtick(pic,"$-2T$", (-2.0,0.0),dir=N,size=0);
xtick(pic,"$-T$", (-1.0,0.0),dir=N,size=0);
xtick(pic,"$T$", (1.0,0.0),dir=N,size=0);
xtick(pic,"$2T$", (2.0,0.0),dir=N,size=0);

void makeplot(picture pic, real s, real tmin, real tmax, pen p=defaultpen) {
  draw(pic,shift(s,0)*graph(sinc, tmin, tmax),p);
}

animation a = animation(global=true);
makeplot(pic, 0, -3, 3);
label(pic,"$x_0$", (0,1),N);
//shipout(pic=pic,prefix="sinc1");
a.add(pic);
label(pic,"$x_1$", (1,1),N,red);
makeplot(pic, 1, -4, 2,red);
//shipout(pic=pic,prefix="sinc2");
a.add(pic);
draw(pic,(0,0)--(0,1),dotted);
//shipout(pic=pic,prefix="sinc3");
a.add(pic);
draw(pic,(1,0)--(1,1),dotted);
//shipout(pic=pic,prefix="sinc4");
a.add(pic);
a.movie(BBox(1mm),delay=150,
        options="-density 300");

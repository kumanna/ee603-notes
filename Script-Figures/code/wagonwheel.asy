import animate;
import graph;
usepackage("animate");
usepackage("amsmath");
settings.render=16;
settings.tex="pdflatex";

animation a = animation(global=true);

picture dumpframe(real t, real omega, string f) {
  picture pic;
  size(pic, 5cm);
  draw(pic, scale(1.2)*unitcircle, invisible);
  draw(pic, unitcircle);
  dot(pic, (cos(-omega * t), sin(-omega * t)),makepen(scale(0.1cm, 0.1cm)*unitcircle) + red);
  label(pic, "$\text{FPS} = f_{\text{wheel}}\times " + f + "$", S, S);
  return pic;
}

int n = 50;
picture pic1, pic2, pic3;
for (int k = 0; k < n; k = k + 1) {
  picture pic;
  save();
  pic1 = dumpframe(k, 2 * pi / n * 5, (string) quotient(n, 5));
  add(pic, pic1.fit(), (0,0), -5cm * E);
  pic2 = dumpframe(k, 2 * pi / n * 25, (string) quotient(n, 25));
  add(pic, pic2.fit(), (0,0), (0,0));
  pic3 = dumpframe(k, 2 * pi / n * 40, "1.25");
  add(pic, pic3.fit(), (0,0), E * 5cm);
  frame f = bbox(pic, white, Fill);
  add(f);
  a.add();
  restore();
}

a.movie(BBox(1mm),delay=10,
        options="-density 300");

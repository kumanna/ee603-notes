import trembling;
import graph;
settings.outformat = "pdf";

real sinc(real t) {
  if (t == 0) {
    return 1;
  }
  return sin(pi * t) / pi / t;
}

size(5cm);
settings.outformat="pdf";

void draw_ctft(int mode=0, path ctft, real[] min_ctft, real[] max_ctft) {
  erase();
  draw(ctft);
  xaxis("$f$",xmin=-4,xmax=4, arrow=Arrow);
  yaxis("$X(f)$", autorotate=false,arrow=Arrow,ymax=2,p=fontsize(8pt));
  if (mode == 0)  {
    shipout(bbox(currentpicture, white, Fill), prefix="ctft1");
    return;
  }

  path rectfilter = (-2,0)--(-2,1.2)--(2,1.2)--(2,0);
  if (mode == 1) {
    draw(rectfilter, gray);
    shipout(bbox(currentpicture, white, Fill), prefix="ctft2");
    return;
  }

  real itimes[] = times(ctft, -2);

  draw(subpath(ctft, 0, itimes[0]), p=linewidth(1)+white);
  draw(subpath(ctft, 0, itimes[0]), dashed);
  itimes = times(ctft, 2);
  draw(subpath(ctft, itimes[0], max_ctft[0]), p=linewidth(1)+white);
  draw(subpath(ctft, itimes[0], max_ctft[0]), dashed);
  draw(rectfilter, gray);
  labelx("$-W$", -2);
  labelx("$W$", 2);
  shipout(bbox(currentpicture, white, Fill), prefix="ctft3");
}

tremble tr=tremble(angle=20,frequency=0.1,random=50,fuzz=1);
real minval = 0.2;
real minx = -3.8;
path ctft = (-3.8,0.2){(0,0)}..{(0,0)}(0,1){(0,0)}..{(0,0)}(3.8,minval);
ctft = tr.deform(ctft);
real[] min_ctft = mintimes(ctft);
real[] max_ctft = maxtimes(ctft);

draw_ctft(0, ctft, min_ctft, max_ctft);
draw_ctft(1, ctft, min_ctft, max_ctft);
draw_ctft(2, ctft, min_ctft, max_ctft);

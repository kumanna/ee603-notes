import animate;
import graph;
settings.tex="pdflatex";

picture make_impulse_train(real T = 1, bool freq_mode=false) {
  picture pic1;
  size(pic1, 10cm);
  int axislength = 4;
  int xmax = ceil(axislength / T);
  string xlabel = "$t$";
  string ylabel = "$s(t)$";
  if (freq_mode) {
    xlabel = "$f$";
    ylabel = "$X(f)$";
  }
  xaxis(pic1, xlabel,xmin=-axislength,xmax=axislength,arrow=Arrow(DefaultHead));
  yaxis(pic1, ylabel,ymin=0,ymax=2,arrow=Arrow(DefaultHead), autorotate=false);
  draw(pic1,(0,1)--(T,1),
       bar=Bars,
       L=Label("$f_s=\frac{1}{T}$",position=MidPoint),
       fontsize(6pt),
       align=2N);

  real level = 0.5;
  for (int i = -xmax+1;i <= xmax-1; ++i) {
    string l = "$" + ((string) i) + "T$";
    if (freq_mode) l = "$\frac{" + ((string) i) + "}{T}$";
    if (i == -1) {
      l = freq_mode ? "$-\frac{1}{T}$" : "$-T$";
    }
    else if (i == 1) {
      l = freq_mode ? "$\frac{1}{T}$" : "$T$";
    }
    else if (i == 0) {
      l = "$0$";
    }
    label(pic1, l, (i*T,0),S,fontsize(6pt));
    //draw(pic1, (i*T,0)--(i*T,1),Arrow(TeXHead));
    path p = (i*T-level,0)--(i*T,1)--(i*T+level,0);
    path p_next = shift((T,0))*p;
    draw(pic1, p);
    if (i*T+level > (i + 1) * T-level) {
      real ipoints[] = intersect(p, p_next);
      fill(pic1,((i+1)*T-level,0)--point(p,ipoints[0])--(i*T+level,0)--cycle,red);
    }
    xtick(pic1, (i*T,0), dir=S, size=1);
  }
  label(pic1, "$W$", (level,0),S,fontsize(6pt));
  label(pic1, "$-W$", (-level,0),S,fontsize(6pt));
  clip(pic1, (-axislength,-3cm)--(axislength,-3cm)--(axislength,2)--(-axislength,2)--cycle);
  return pic1;
}

animation a = animation(global=true);

//real factors[] = {0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2, 1.3, 1.4};
int n = 40;
real factors[] = new real[2 * n];
real delta = (1.4 - 0.3) / n;
for (int i = 0; i < n; ++i) {
  factors[i] = 0.6 + i * delta;
  factors[2 * n - i - 1] = factors[i];
}
for (int i = 0; i < factors.length; i = i + 1) {
  save();
  picture pic2 = make_impulse_train(1 / factors[(i + quotient(factors.length, 2)) % factors.length], true);
  frame f = bbox(pic2, white, Fill);
  add(f);
  a.add();
  restore();
}

a.movie(BBox(1mm),delay=150,
        options="-density 300");

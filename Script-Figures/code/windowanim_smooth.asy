import graph;
settings.outformat="pdf";

defaultpen(fontsize(8pt));
typedef real realfcn(real);
realfcn sinc(real values[]) {
  return new real(real t) {
    real retval = 0;
    for (int i = 0; i < values.length; ++i) {
      if (t - i == 0) {
        retval = retval + values[i];
      }
      else {
        retval = retval + values[i] * sin(pi * (t - i)) / pi / (t - i);
      }
    }
    return retval;
  };
}

void makeplot(picture pic=currentpicture, real s, real tmin, real tmax, pen p=defaultpen, real A[]={1.0, 0.0}) {
  draw(pic,shift(s,0)*graph(sinc(A), tmin, tmax),p);
}

size(8cm,4cm,IgnoreAspect);
xaxis("$t$", -1.2, 7.0, Arrow);
xtick("$0$", (0.0,0.0),dir=N,size=0);
xtick("$-T$", (-1.0,0.0),dir=N,size=0);
xtick("$T$", (1.0,0.0),dir=N,size=0);
xtick("$2T$", (2.0,0.0),dir=N,size=0);
xtick("$3T$", (3.0,0.0),dir=N,size=0);
xtick("$4T$", (4.0,0.0),dir=N,size=0);
xtick("$5T$", (5.0,0.0),dir=N,size=0);
xtick("$6T$", (6.0,0.0),dir=N,size=0);


real A[] = {1.5, 0.0};
makeplot(0, -1, 6, invisible, A=A);
real values[] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
int window_length = values.length;
int N = 101;
// dot((0,1));
// draw((0,0)--(0,1),dotted);
int imgno = 0;
for (int j = 0; j < window_length; ++j) {
  for (int i = 0; i < N; ++i) {
    save();
    values[j] = i / (N - 1);
    makeplot(0, -1, 6,red,values);
    for (int k = 0; k < values.length; ++k) {
      dot((k,values[k]), red);
      draw((k,0)--(k,values[k]),dotted+red);
    }
    string nstr = format("%.1f", values[0]);
    if (nstr == "1") nstr = "";
    string labelstr = "$" + nstr + "\delta[n]";
    for (int k = 1; k < values.length; ++k) {
      if (values[k] > 1 / 2 / (N - 1)) {
        nstr = format("%.1f", values[k]);
        if (nstr == "1") nstr = "";
        labelstr = labelstr + "+" + nstr + "\delta[n-" + ((string)k) + "]";
      }
    }
    labelstr = labelstr + "$";
    label(scale(0.6)*Label(labelstr), (0,1.3),E, fontsize(8pt));
    shipout("p_" + replace(format("%4d", ++imgno), " ", "0"));
    write(imgno);
    restore();
  }
}

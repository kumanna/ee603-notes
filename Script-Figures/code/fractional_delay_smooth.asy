import graph;

size(6cm);
settings.tex = "pdflatex";

typedef real realfcn(real);
realfcn sinc(real values[]) {
  return new real(real t) {
    real retval = 0;
    for (int i = 0; i < values.length; ++i) {
      if (t - i == 0) {
        retval = retval + values[i];
      }
      else {
        retval = retval + values[i] * sin(pi * (t - i)) / pi / (t - i);
      }
    }
    return 2 * retval;
  };
}

real A[] = {1.0};
real tmin = -4;
real tmax = 4;
draw(graph(sinc(A), tmin, tmax), gray+dashed);
xaxis("$t$", -4.5, 4.5, Arrow);
draw((-4.5,-1)--(4.5,-1)--(4.5,2.8)--(-4.5,2.8)--cycle,invisible);

int FRAMES = 200;
int imgno = 0;
for (int f = 0; f <= FRAMES; f = f + 1) {
  save();
  real offset = ((real)f) / FRAMES;
  for (int i = -4; i <= 3; i = i + 1) {
    real tvals[] = {1.0};
    real yval = sinc(tvals)(i + offset);
    draw((i + offset, 0)--(i + offset, yval));
    dot((i + offset, yval));
  }
  string offsetstr = format("%.2f", offset);
  if (length(offsetstr) < 4) offsetstr = offsetstr + "0";
  if (offset == 0) { offsetstr = "0.00"; }
  if (offset == 1) { offsetstr = "1.00"; }
  string labelstr = "$t = nT + " + offsetstr + "T, \quad x[n] = \frac{\sin(\pi(n - " + offsetstr + "))}{\pi(n - " + offsetstr + ")}$";
  label(scale(0.6)*Label(labelstr), (0,2.5),(0,0), fontsize(8pt));
  shipout("fdelay_" + replace(format("%4d", ++imgno), " ", "0"));
  write(imgno);
  restore();
}

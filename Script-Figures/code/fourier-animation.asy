import graph;
import animate;
usepackage("animate");
settings.render=16;
settings.tex="pdflatex";


import graph;

typedef real realfcn(real);

realfcn sinc(real T) {
  return new real(real x) {
    if (x == 0)
      return 1;
    return sin(pi * x / T) / pi / (x / T);
  };
}

realfcn rect(real T) {
  return new real(real x) {
    if (abs(x) < 0.5 / T) return T / T;
    return 0;
  };
}

animation a = animation(global=true);

void dumpframe(real T) {
  save();
  picture pic1;
  size(pic1, 10cm);
  real xmax = 3.7;
  xaxis(pic1, "$t$",xmin=-xmax,xmax=xmax,arrow=Arrow(TeXHead));
  yaxis(pic1, "$x(t)$",ymin=-0.3,ymax=1.3,arrow=Arrow(TeXHead),autorotate=false);
  label(pic1, "$0$",(0,0),SE);
  label(pic1, "$T = " + ((string) T) + "$",(T,0),N,red+fontsize(6pt));
  for (int i = 1; i <= (int) xmax; i = i + 1) {
    label(pic1, "$" + ((string) i) + "$",(i,0),S);
    label(pic1, "$-" + ((string) i) + "$",(-i,0),S);
    xtick(pic1,(i,0), dir=S, size=0.08cm);
    xtick(pic1,(-i,0), dir=S, size=0.08cm);
  }

  draw(pic1, graph(sinc((T)), -3.2, 3.2, operator ..), red);
  add(pic1.fit(),(0,0),1cm * W);

  picture pic2;
  size(pic2, 3cm);
  real xmax = 1.1;
  xaxis(pic2, "$f$",xmin=-xmax,xmax=xmax,arrow=Arrow(TeXHead));
  yaxis(pic2, "$X(f)$",ymin=-0.1,ymax=1.5,arrow=Arrow(TeXHead),autorotate=false);
  label(pic2, "$0$",(0,0),SE);
  label(pic2, "$\frac{1}{2T}$",(0.5/(T),0),S);
  label(pic2, "$-\frac{1}{2T}$",(-0.5/(T),0),S);
  label(pic2, "$T = " + ((string) T) + "$", (0,1),NE,fontsize(8pt));
  draw(pic2, graph(rect(T), -1.1, 1.1, operator --, n=400), blue);
  add(pic2.fit(),(0,0),1cm * E);
  draw((-1cm,0)--(1cm,0),arrow=Arrows(TeXHead));
  a.add();
  restore();
}

real T[] = {0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95, 1.0, 1.05, 1.1, 1.15, 1.2, 1.25, 1.3, 1.35, 1.4, 1.45, 1.5, 1.55};
for (int k = 0; k < T.length; k = k + 1) {
  dumpframe(T[k]);
}
for (int k = 0; k < T.length; k = k + 1) {
  dumpframe(T[T.length - k - 1]);
}

//a.movie();
a.movie(BBox(1mm),delay=150,
        options="-density 300");

// import graph;

// // Uncomment the following 2 lines to support pdf animations:
// usepackage("animate");
// settings.outformat="pdf";
// settings.tex="pdflatex";
// settings.render = 16;

// import animation;

// size(0,200);

// defaultpen(3);
// dotfactor=4;

// pair wheelpoint(real t)
// {
//   return (t+cos(t),-sin(t));
// }

// guide wheel(guide g=nullpath, real a, real b, int n)
// {
//   real width=(b-a)/n;
//   for(int i=0; i <= n; ++i) {
//     real t=a+width*i;
//     g=g--wheelpoint(t);
//   }
//   return g;
// }

// real t1=0;
// real t2=t1+2*pi;

// animation a = animation(global=true);

// draw(circle((0,0),1));
// draw(wheel(t1,t2,100),linetype("0 2"));
// yequals(Label("$y=-1$",1.0),-1,extend=true,linetype("4 4"));
// xaxis(Label("$x$",align=3SW),0);
// yaxis("$y$",0,1.2);
// pair z1=wheelpoint(t1);
// pair z2=wheelpoint(t2);
// dot(z1);
// dot(z2);

// int n=10;
// real dt=(t2-t1)/n;
// for(int i=0; i <= n; ++i) {
//   save();

//   real t=t1+dt*i;
//   draw(circle((t,0),1),red);
//   dot(wheelpoint(t));

//   a.add(); // Add currentpicture to animation.
//   restore();
// }

// for(int i=n-1; i >= 0; --i) {
//   save();

//   real t=t1+dt*i;
//   draw(circle((t,0),1),red);
//   dot(wheelpoint(t));

//   a.add(); // Add currentpicture to animation.
//   restore();
// }

// erase();

// // Merge the images into a gif animation.
// //a.movie(BBox(0.25cm),loops=10,delay=250);

// a.movie(delay=250);
// // Merge the images into a pdf animation.
// //label(a.pdf(BBox(0.25cm),delay=250,"controls",multipage=true));

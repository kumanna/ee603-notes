import graph;

settings.outformat="pdf";

defaultpen(fontsize(8pt));
typedef real realfcn(real);
realfcn sinc(real values[]) {
  return new real(real t) {
    real retval = 0;
    for (int i = 0; i < values.length; ++i) {
      if (t - i == 0) {
	retval = retval + values[i];
      }
      else {
	retval = retval + values[i] * sin(pi * (t - i)) / pi / (t - i);
      }
    }
    return retval;
  };
}

void makeplot(picture pic=currentpicture, real s, real tmin, real tmax, pen p=defaultpen, real A[]={1.0, 0.0}) {
  draw(pic,shift(s,0)*graph(sinc(A), tmin, tmax),p);
}

size(8cm,4cm,IgnoreAspect);
xaxis("$t$", -3.2, 3.2, Arrow);
xtick("$0$", (0.0,0.0),dir=N,size=0);
xtick("$-2T$", (-2.0,0.0),dir=N,size=0);
xtick("$-T$", (-1.0,0.0),dir=N,size=0);
xtick("$T$", (1.0,0.0),dir=N,size=0);
xtick("$2T$", (2.0,0.0),dir=N,size=0);


makeplot(0, -3, 3, gray);
label("$x_0$", (0,1),NW);
real values[] = {1.0, 0.0};
int N = 50;
dot((0,1));
draw((0,0)--(0,1),dotted);
for (int i = 0; i < 2 * N + 1; ++i) {
  save();
  values[1] = (i > N ? (2 * N - i) : i) / N / 2;
  label("$x_1$", (1,values[1]),NE,red);
  makeplot(0, -4, 3,red,values);
  dot((1,values[1]), red);
  draw((1,0)--(1,values[1]),dotted+red);
  shipout("p" + ((string) (i + 1)));
  restore();
}

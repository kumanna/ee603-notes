settings.outformat="svg";

picture genpicture(bool quantized) {
  picture pic;
  size(pic,6cm);
  pair[] points = {(0,1.2), (1,2.3), (2,3.6), (3,2.9)};
  pair[] points_q = {(0,1), (1,2), (2,4.0), (3,3.0)};
  int j;
  for (j=0; j<points.length; j=j+1) {
    draw(pic,(points[j].x,0)--points[j]);
    dot(pic,points[j]);
    label(pic,"$" + ((string) j) + "$", (j,0),align=S,fontsize(12pt));
    if (quantized) {
      draw(pic,(points_q[j].x,0)--points_q[j], blue);
      dot(pic,points_q[j], blue);
    }
  }
  draw(pic,(0,0)--(4,0),Arrow(TeXHead));
  draw(pic,(0,0)--(0,5),Arrow(TeXHead));
  label(pic, "$n$", (4,0),align=E,fontsize(12pt));
  label(pic, "$x[n]$", (0,5),align=N,fontsize(12pt));

  for (j = 0; j < 5; j=j+1) {
    draw(pic,(0,j)--(4,j),dotted);
    if (j > 0)
      label(pic,"$" + ((string) j) + "$", (0,j),align=W,fontsize(12pt));
  }

  return pic;
}

picture pic = genpicture(false);
shipout(prefix="stemplot", pic=pic);
picture pic = genpicture(true);
shipout(prefix="stemplot2", pic=pic);


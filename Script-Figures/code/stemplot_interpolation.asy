settings.outformat="svg";

// MODES
// 0: bare stem plot
// 1: sample-and-hold
// 2: linar interpolation
// 3: sinc interpolation
picture genpicture(int mode = 0) {
  picture pic;
  size(pic,6cm);
  pair[] points = {(0,1.2), (1,2.3), (2,3.6), (3,2.9)};
  pair[] points_q = {(0,1), (1,2), (2,4.0), (3,3.0)};
  int j;
  for (j=0; j<points.length; j=j+1) {
    draw(pic,(points[j].x,0)--points[j]);
    dot(pic,points[j]);
    label(pic,"$" + ((string) j) + "$", (j,0),align=S,fontsize(12pt));
    if (mode == 1) {
      draw(pic,points[j]--shift(1,0)*points[j], red+dashed+linewidth(1));
    }
    else if (mode == 2) {
      real left_yval = (j > 0) ? points[j - 1].y : 0;
      real right_yval = (j < points.length - 1) ? points[j + 1].y : 0;
      draw(pic,points[j]--(points[j].x+1,right_yval), red+dashed+linewidth(1));
    }
  }
  if (mode == 2) {
    draw(pic,(points[0].x-1,0)--points[0], red+dashed+linewidth(1));
  }

  draw(pic,(-1,0)--(4,0),Arrow(TeXHead));
  draw(pic,(0,0)--(0,5),Arrow(TeXHead));
  label(pic, "$n$", (4,0),align=E,fontsize(12pt));
  label(pic, "$x[n]$", (0,5),align=N,fontsize(12pt));

  for (j = 0; j < 5; j=j+1) {
    draw(pic,(0,j)--(4,j),dotted);
    if (j > 0)
      label(pic,"$" + ((string) j) + "$", (0,j),align=W,fontsize(12pt));
  }

  return pic;
}

picture pic = genpicture(0);
shipout(prefix="stemplot_interpolation", pic=pic);
picture pic = genpicture(1);
shipout(prefix="stemplot_interpolation_sample_and_hold", pic=pic);
picture pic = genpicture(2);
shipout(prefix="stemplot_interpolation_linear_interpolation", pic=pic);

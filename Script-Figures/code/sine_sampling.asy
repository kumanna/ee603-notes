import graph;
settings.outformat = "pdf";

real mysin(real t) {
  return sin(t);
}

picture draw_base_picture(int mode) {
  erase();
  picture pic;
  size(pic, 10cm);
  draw(pic, graph(mysin, 0, 7));
  xaxis(pic, "$t$",xmin=-0.2,xmax=7);
  yaxis(pic, "$\sin(2\pi t)$", autorotate=false);
  xtick(pic, "$0.5$", (1.0pi,0));
  xtick(pic, "$1.0$", (2.0pi,0));
  if (mode == 0) {
    label(pic, "$f_s = ?$ Hz", (pi, 1));
  }
  if (mode == 1) {
    label(pic, "$f_s = 2$ Hz", (pi, 1));
    dot(pic, (0,mysin(0)), red);
    dot(pic, (pi, mysin(pi)), red);
    dot(pic, (2 * pi, mysin(2 * pi)), red);
  }

  if (mode == 2) {
    label(pic, "$f_s = 2.1$ Hz", (pi, 1));
    dot(pic, (0,mysin(0)), red);
    dot(pic, (pi * 2 / 2.1, mysin(2 * pi / 2.1)), red);
    dot(pic, (4 * pi / 2.1, mysin(2 * pi * 2 / 2.1)), red);
  }

  if (mode == 3) {
    label(pic, "$f_s = 7.2$ Hz", (pi, 1));
    for (int i = 0; i < 9; ++i) {
      dot(pic, (i * 2 * pi / 7.2,mysin(i * 2 * pi / 7.2)), red);
    }
  }
  return pic;
}

shipout(bbox(draw_base_picture(0), white, Fill), prefix="sine_sampling_1");
shipout(bbox(draw_base_picture(1), white, Fill), prefix="sine_sampling_2");
shipout(bbox(draw_base_picture(2), white, Fill), prefix="sine_sampling_3");
shipout(bbox(draw_base_picture(3), white, Fill), prefix="sine_sampling_4");

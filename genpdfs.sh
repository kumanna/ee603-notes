for i in *svg;do
    rsvg-convert -f pdf -o ${i%svg}pdf $i
done

for i in *gif;do
    convert $i ${i%gif}pdf
done
